# Sylvain

## Lettre d'émancipation

J'ai déjà écris ma lettre d'amour. Elle s'intitule One Nation.
Il manquait celle de rupture. Ma lettre d'émancipation.

Merci société moderne d'avoir fait de moi qui je suis.
Mais aujourd'hui je vous quitte pour la suite.

L'État providence m'empêche de danser.
Qu'à cela ne tienne, je vais m'en aller.

[Lire la lettre](lettre_emancipation/lettre_emancipation.md)

[Télécharger la lettre en PDF](lettre_emancipation/lettre_emancipation.pdf)

[![](http://img.youtube.com/vi/eoA4IQDQV20/0.jpg)](http://www.youtube.com/watch?v=eoA4IQDQV20 "")

[Voir la lecture en vidéo](http://www.youtube.com/watch?v=eoA4IQDQV20 "")

