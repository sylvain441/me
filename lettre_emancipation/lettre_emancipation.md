# LETTRE D’EMANCIPATION
> Version 1.1 du 02/09/2020

_NB: Cette déclaration n’est pas rédigée en langage juridique. Les mots sont destinés à être interprétés pour l'énergie qu'ils transmettent et selon le langage commun._

[Télécharger la lettre en PDF](lettre_emancipation.pdf)

[![](http://img.youtube.com/vi/eoA4IQDQV20/0.jpg)](http://www.youtube.com/watch?v=eoA4IQDQV20 "")

[Voir la lecture en vidéo](http://www.youtube.com/watch?v=eoA4IQDQV20 "")


## 1. État des lieux

Aujourd’hui, je me reconnais en tant qu’être biologique conscient et animé de vie. Je suis doté d’un corps depuis l’année 1988, d’un esprit et d’une âme éternelle. J’interagis avec le reste du monde sous le nom de « Sylvain » et parfois « Biquette ».

J’ai compris que SYLVAIN CHARLES en MAJUSCULES, est mon identité légale, C’est une entité fictive créée 2 jours après ma naissance. Je ne suis pas cette personne juridique mais je l’administre. La République Française en a la pleine propriété.

J’ai compris que cette République, comme toutes les autres, est une entreprise à but lucratif. Une société commerciale qui transforme les êtres humains en contribuables et valorise chaque centimètre carré du Vivant en marchandise.

J’ai compris que l’agenda de cette société est rythmé par l’imaginaire « spécial » d’investisseurs privés. Que ceux ci n’ont jamais eu comme objectif de mener l’humanité vers l’éveil, ni de faire en sorte que la Terre devienne chaque jour plus agréable à vivre.

J’ai compris que les états, les gouvernements, les institutions à 3 lettres de type ONU, OMC, OMS, NSA, … les grandes ONG, grandes entreprises et grands médias n’ont que le pouvoir que nous-mêmes et les flux financiers veulent bien leur accorder. 

J’ai compris que la puissance est entre les mains de ceux qui se sont accaparés le pouvoir de création monétaire. Les États ne sont réduits qu'au triste rôle de collecteur de taxes et de temps humain. Leur population est subtilement mise en esclavage pour rembourser une dette artificielle.

J’ai compris que la République Française s’est accordée mon consentement par tromperie. Je me rends compte qu’en jouant à ce jeu biaisé, j’ai cautionné des horreurs commises contre l’humanité. Dois-je vraiment les nommer ?

J’ai compris que rester sous cette juridiction m’obligé à négocier avec mon cœur, il n’en est plus question.

Je ne remets pas en cause l’idée de société comme outil pour organiser la vie sociale, je remets simplement en question et refuse cette vision de la vie en communauté.

Je suis une poussière d’étoile dotée de la conscience et du libre arbitre. Pour sortir de ce mauvais rêve il suffit que je me réveille. Ainsi, aujourd’hui, je prends mes responsabilités et me retire.

Merci d’avoir fait de moi qui je suis. Je quitte en paix ce jeu de société et pars à l’aventure sur mon chemin de vie. 

J’abandonne de fait mes droits de citoyens et par la même occasion je m’acquitte avec joie de mes devoirs d’esclave moderne.

Je suis libre de créer ou d’adopter la juridiction qui me représente afin de ne plus avoir à me retrouver en position désobéissante. 

Je m’en remets aux lois immuables de l’Univers que nul ne peut transgresser sans conséquences. 

J’ai pris l’engagement envers moi-même de vivre selon les termes du Manifeste One Nation. C’est pourquoi je me retrouve aujourd’hui à faire cette déclaration.

Je choisis de m’émanciper du monde légal et commercial. Pour cela je prends quelques dispositions.

## 2. Une histoire de consentement

Dès aujourd’hui, je ne consens plus à perdre ma vie pour la gagner.

Je ne consens plus à cette vision du monde où tout est commerce y compris la Nature et l’énergie humaine.

Je retire à toute autorité illégitime le droit de décider pour moi-même ce qui est bon et ce qui est mauvais.

Je suis le seul habilité à choisir comment vivre. Comment me nourrir, respirer, me soigner, m’exprimer, organiser mon quotidien et me déplacer. Dès aujourd’hui je choisis librement comment faire usage de mon temps de vie et comment je souhaite échanger avec mes semblables.

Je n’octroie à personne le droit de porter atteinte à mon intégrité physique, morale et mentale contre mon gré.

Je refuse toute tentative de m’imposer par la force, la contrainte ou la malice l’adhésion à la pensée unique. Je suis libre de penser et de changer d’opinion. Ma liberté de parole et d’entreprendre sont des droits sacrés, inaliénables et valables en tout point du globe.

Je retire mon consentement concernant le stockage à durée illimitée de mon activité en sur Terre et en ligne. Le Sylvain de hier n’est plus le Sylvain d’aujourd’hui et encore moins celui de demain. Mon intimité est un temple sacré. Nul n'est autorisé à la violer. 

Je ne consens plus à consacrer une seule seconde de mon temps de vie sur Terre à une activité contrainte et forcée.

Je ne me reconnais pas dans cette entité légale nommée SYLVAIN CHARLES créée au début de ma vie sans mon consentement éclairé. Je retire ce consentement tacite obtenu par tromperie et désengage l’Être Humain que je suis de toute action engagée par le passé en tant que citoyen manipulé et ignorant. Je reste bien évidemment responsable devant l’univers pour toute action ou parole ayant créé du tort à autrui.

Mes actes, paroles et pensées relèvent de ma responsabilité uniquement. Ainsi nul n'est autorisé à interférer négativement sur la vie d’autrui pour ce que j’ai pu dire, penser et faire sur Terre depuis ma naissance jusqu’à ma mort.

Je souhaite quitter le monde légal dans les règles et proprement. Je ne consens à aucune intimidation ou dérangement qui empêcherait la fluidité de ce processus d’émancipation.

Toute nouvelle dette émise envers ma personnalité juridique sous forme de taxe, d’impôt ou d’amende sera considérée comme hors-contrat.

Entre maintenant et mon émancipation totale, je serai peut être amené à utiliser les instruments du pouvoir tel que l’argent ou la nationalité française. Ceci afin de maintenir un niveau de confort et de dignité suffisant à l’être biologique que je suis. Cela n’induit en rien mon consentement envers toutes les malfaçons perpétrées grâce à ces outils.

Je me désengage de tout contrat avec l’Église Catholique. Je n’ai besoin d’aucun intermédiaire entre moi et la Source de Vie. Ainsi le chef du VATICAN n’a plus aucune procuration sur mon âme, en tout point de l’espace et du temps.

N’ayant pas d’avion, je compte faire librement usage des routes qui brisent la continuité harmonieuse du paysage. Plus aucun agent n’a d’autorité pour m’empêcher de faire valoir mon droit sacré et imprescriptible de me déplacer par tous les moyens qui me semblent appropriés.

Je compte faire usage de mon droit inaliénable de choisir un lieu de vie qui soutient mon existence physique et porte les valeurs que je souhaite incarner sur Terre. Ce lieu de vie sera retiré de la juridiction du pays sur lequel il se trouvera.

Pour résumer : seuls les contrats signés avec bienveillance et consentement éclairé de toutes les parties ont une valeur à mon cœur.

## 3. Ma juridiction : One Nation

Je me reconnais totalement dans le Manifeste One Nation rédigé en langue française le 29 février 2020 en Armorique :

###### Considérant
Que la Terre est un grand organisme vivant.
Qu’une infinité de formes de vie y cohabitent en interdépendance.
Que l’humanité a une influence majeure sur les autres règnes terrestres.
Que chaque être humain est une cellule unique et individualisée de la Terre.
Que la résilience d’un organisme est maintenue lorsque ses cellules sont libres de jouer pleinement leurs rôles au service du Tout et de ses parties.

###### Je proclame
Ma gratitude envers la Vie qui anime toutes choses.
Mon respect profond envers tous les habitants de notre vaisseau terrestre.
La primauté de l'être biologique et conscient que je suis, sur l'entité légale créée après ma naissance.
La légitimité de mon pouvoir créateur tant qu’il exclut toute forme de vol, tromperie, violence morale ou physique.
La responsabilité absolue de mes pensées, paroles et actions engendrées par mon libre-arbitre.

###### Je m'engage à
Écarter la peur de mon esprit pour que dans l’amour, je puisse prendre soin de moi et de tout ce qui vit.
Me saisir de mon pouvoir d’être humain vivant pour l’employer au service de l’harmonie.
Recourir à la désobéissance joyeuse et pacifique, lorsque les droits fondamentaux du vivant sont en danger.

_Je reconnais ce pacte qui me lie à moi-même et à la Terre. Je rejoins par le cœur les Êtres émancipés de One Nation par delà les océans et les continents._

## 4. Quelques précisions

Ce Manifeste pose le cadre d’une cohabitation harmonieuse en interdépendance. Ainsi nul besoin d’avoir recours à la création de textes de loi et d’institutions faillibles chargées de les appliquer. 

Toute personne se reconnaissant véritablement dans ce texte reçois ma confiance absolue.

Je tiens à préciser qu’il n’y a en moi aucune haine envers ce système décadent. Je ne souhaite convaincre quiconque contre son gré de la stérilité de cet environnement.

J’éprouve une gratitude profonde envers tout ce qui a fait de moi qui je suis aujourd’hui.

Je suis heureux et en bonne santé. Je prend soin chaque jour de ma santé mentale et physique. Je n’ai aucune raison de tomber gravement malade et encore moins de mettre fin à la vie de mon corps physique.

Je ne compte pas utiliser ma nouvelle liberté pour engendrer du mal sur Terre car c’est incompatible avec ma promesse de mourir sans regret et celle de laisser l’endroit plus beau que je l’ai trouvé.

Si par manque de vigilance, je me retrouve un jour à essayer d’imposer ma vision à autrui contre son gré, je consens à ce que l’on m’incite (avec bienveillance) à retrouver le droit chemin.

À noter que je ne suis pas exempt de contradictions ni de paradoxes. La quête de cohérence est d’ailleurs le carburant qui me met en mouvement chaque jour. Merci d’avance pour toute contribution me poussant dans cette direction.

Merci d’exister et de m’avoir lu jusqu’au bout.  
Avec tout mon amour…  
Très belle journée  

— Sylvain